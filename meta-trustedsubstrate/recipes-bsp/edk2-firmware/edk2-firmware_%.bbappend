# Enable building both u-boot and edk2-firmware
PROVIDES:remove = "virtual/bootloader"

# Include machine specific configurations for UEFI EDK2
MACHINE_EDK2_REQUIRE ?= ""
MACHINE_EDK2_REQUIRE:qemuarm64-cca = "qemuarm64-cca.inc"

# depend on optee-stmm machine feature?
STMM_EDK2_REQUIRE = "${@bb.utils.contains('MACHINE_FEATURES', 'optee-stmm', 'stmm.inc', '', d)}"

# meta-arm doesn't export that for armv7
export GCC5_ARM_PREFIX = "${TARGET_PREFIX}"

require ${STMM_EDK2_REQUIRE}

COMPATIBLE_MACHINE:synquacer = "synquacer"
COMPATIBLE_MACHINE:rockpi4b = "rockpi4b"

require ${MACHINE_EDK2_REQUIRE}
