# Generate U-Boot for imx8mp verdin

FILESEXTRAPATHS:prepend := "${THISDIR}/u-boot/${MACHINE}:"

DEPENDS += "imx-boot-firmware-files trusted-firmware-a"

SRC_URI += "file://${MACHINE}.cfg \
    file://0001-imx8mp-avoid-to-inject-optee-node.patch \
"

UBOOT_BOARDDIR = "${KCONFIG_CONFIG_ROOTDIR}"
UBOOT_ENV_NAME = "imx8mp-verdin.env"

do_compile:prepend() {
    # meta-freescale's imx-boot-firmware-files recipe installs the firmware
    # files using do_deploy() and set the noexec flag to do_install()
    cp ${DEPLOY_DIR_IMAGE}/lpddr4_pmu_train_*.bin \
       ${DEPLOY_DIR_IMAGE}/bl31.bin \
    ${KCONFIG_CONFIG_ROOTDIR}
}

do_compile[depends] += "\
    imx-boot-firmware-files:do_deploy \
    trusted-firmware-a:do_deploy \
"

# Avoid to strip mkimage copied to /sysroot-only
INHIBIT_SYSROOT_STRIP = "1"

do_install:append() {
    install -D -p -m 0644 ${DEPLOY_DIR_IMAGE}/signed_hdmi_imx8m.bin \
        ${D}/sysroot-only/firmware/signed_hdmi_imx8m.bin
    cp ${KCONFIG_CONFIG_ROOTDIR}/arch/arm/dts/*imx8mp-verdin*.dtb \
       ${KCONFIG_CONFIG_ROOTDIR}/bl31.bin \
       ${KCONFIG_CONFIG_ROOTDIR}/lpddr4_pmu_train_*.bin \
       ${KCONFIG_CONFIG_ROOTDIR}/spl/u-boot-spl.bin \
       ${KCONFIG_CONFIG_ROOTDIR}/tools/mkimage \
       ${KCONFIG_CONFIG_ROOTDIR}/u-boot-nodtb.bin \
    ${D}/sysroot-only/firmware
}
