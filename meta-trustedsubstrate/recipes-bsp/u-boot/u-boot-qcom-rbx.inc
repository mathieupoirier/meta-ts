# Generate Android boot images for Qualcomm boards.

# There is a tag associated with this commit, so it will never be garbage collected
SRCREV = "8b8e36f303cb5fc2be6cd492f1243eb2dcd38d7d"
# Qualcomm U-Boot support is still going through a lot of big changes, with many patches
# needed for the RBx boards still in the process of being upstreamed.
# This integration branch will always be ahead of qcom-next and is rebased regularly,
# as a result it is non-trivial to backport the patches to whatever version of U-Boot TRS
# is using.
SRC_URI:remove = "git://source.denx.de/u-boot/u-boot.git;protocol=https;branch=master"
SRC_URI =+ "git://git.codelinaro.org/linaro/qcomlt/u-boot;protocol=https;branch=caleb/rbx-integration"
# This patch doesn't apply without fuzz (and isn't vital) so drop it.
SRC_URI:remove = "file://0005-bootefi-Call-the-EVT_DT_NODE_PROP_PURGE-event-handle.patch"
COMPATIBLE_MACHINE = "qcom-rbx"

# We pack U-Boot into an Android boot image.
DEPENDS += "android-tools-native"

UBOOT_BOARDDIR = "${S}/board/qualcomm"

QCOM_RBX_DEVICES = "rb1:qrb2210-rb1 rb2:qrb4210-rb2 rb3:sdm845-db845c rb5:qrb5165-rb5"

# Qualcomm boards chainload U-Boot by packing it into an Android boot image.
# Do that here, generating a boot image for each board.
do_compile:append() {
	# Upstream mkbootimg is dumb and breaks if you don't give it a ramdisk.
	touch ${B}/empty-ramdisk
	cat ${B}/qcom_defconfig/u-boot-nodtb.bin | gzip - > ${B}/u-boot-nodtb.bin.gz
	for device in ${QCOM_RBX_DEVICES}; do
		BOARD=${device%:*}
		DTB=${device#*:}

		# Generate the boot image
		cat ${B}/u-boot-nodtb.bin.gz > ${B}/u-boot-${BOARD}.bin.gz
		cat ${B}/qcom_defconfig/dts/upstream/src/arm64/qcom/${DTB}.dtb \
			>> ${B}/u-boot-${BOARD}.bin.gz
		mkbootimg --base 0x0 --kernel_offset 0x8000 \
			--ramdisk_offset 0x01000000 \
			--tags_offset 0x100 \
			--pagesize 4096 \
			--ramdisk ${B}/empty-ramdisk \
			--kernel ${B}/u-boot-${BOARD}.bin.gz \
			--output ${B}/u-boot-${BOARD}.img
	done
}

do_deploy:append() {
	for device in ${QCOM_RBX_DEVICES}; do
		BOARD=${device%:*}
		install -m 0644 ${B}/u-boot-${BOARD}.img ${DEPLOYDIR}/u-boot-${BOARD}.img
	done
}
