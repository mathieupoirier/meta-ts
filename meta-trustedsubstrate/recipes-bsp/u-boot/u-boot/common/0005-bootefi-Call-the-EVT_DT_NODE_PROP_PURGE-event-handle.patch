From 6c1f4c77272b19dd4a9e753cb1580e283cec7a74 Mon Sep 17 00:00:00 2001
From: Sughosh Ganu <sughosh.ganu@linaro.org>
Date: Mon, 5 Feb 2024 14:03:51 +0530
Subject: [PATCH] bootefi: Call the EVT_DT_NODE_PROP_PURGE event handler

The bootefi command passes the devicetree to the kernel through the
EFI config table. Call the event handler for fixing the devicetree
before jumping into the kernel. This removes any devicetree nodes
and/or properties that are specific only to U-Boot, and are not to be
passed to the OS.

Signed-off-by: Sughosh Ganu <sughosh.ganu@linaro.org>
---
 lib/efi_loader/efi_helper.c |   19 +++++++++++++++++++
 1 file changed, 19 insertions(+)

Upstream-Status: Pending

--- a/lib/efi_loader/efi_helper.c
+++ b/lib/efi_loader/efi_helper.c
@@ -380,6 +380,24 @@ done:
 }
 
 /**
+ * event_notify_dt_purge() - call dt_purge event
+ *
+ * @fdt:       address of the device tree to be passed to the kernel
+ *             through the configuration table
+ * Return:     None
+ */
+static void event_notify_dt_purge(void *fdt)
+{
+	int ret;
+	struct event_dt_node_prop_purge fixup = {0};
+
+	fixup.fdt = fdt;
+	ret = event_notify(EVT_DT_NODE_PROP_PURGE, &fixup, sizeof(fixup));
+	if (ret)
+		printf("Error: %d: DT Purge event failed\n", ret);
+}
+
+/**
  * efi_get_configuration_table() - get configuration table
  *
  * @guid:	GUID of the configuration table
@@ -478,6 +496,7 @@ efi_status_t efi_install_fdt(void *fdt)
 	efi_carve_out_dt_rsv(fdt);
 
 	efi_try_purge_kaslr_seed(fdt);
+	event_notify_dt_purge(fdt);
 
 	if (CONFIG_IS_ENABLED(EFI_TCG2_PROTOCOL_MEASURE_DTB)) {
 		ret = efi_tcg2_measure_dtb(fdt);
