FSBL Binaries
#############

We are using a precompiled binaries for their fsbl

Unfortunately the Xilinx code requires their custom cross compiler
1. Download http://petalinux.xilinx.com/sswreleases/rel-v2022/xsct-trim/xsct-2022-1.tar.xz and extract it
2. add PATH="$HOME/Downloads/Vitis/2022.1/gnu/aarch64/lin/aarch64-none/bin/:$PATH to your .bashrc
3. clone https://github.com/Xilinx/embeddedsw.git
4. cd xilinx/embeddedsw/lib/sw_apps/zynqmp_fsbl/src
5. make clean all BOARD=som -> copy fsbl.elf to meta-trustedsubstrate/recipes-bsp/u-boot/u-boot/zynqmp-kria-starter/zynqmp_fsbl.elf

Image signing
#############

Since we don't want to program eFUSEs we are using a pseudo authentication mode
described as 'bootheader authentication' in Xilinx docs [0].
In this mode it is possible to validate the authentication process without
programming key hash to eFUSE. In this mode RSA authentication of the boot
image(s) is done excluding the verification of PPK hash and SPK ID.
Once authentication process is validated, user can program the eFUSE keys and
use appropriate BIF files to generate the image.

primary.pen and secondary.pem in this folder as generated using [1]

More links for image signing can be found
- https://docs.xilinx.com/r/en-US/ug1283-bootgen-user-guide/Introduction
- https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841708/Zynq+Ultrascale+MPSoC+Security+Features#ZynqUltrascale
- https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842432/ZynqMp+security+features+usage+in+u-boot

[0] https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841708/Zynq+Ultrascale+MPSoC+Security+Features#ZynqUltrascale%2BMPSoCSecurityFeatures-BootheaderAuthentication
[1] https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841708/Zynq+Ultrascale+MPSoC+Security+Features#ZynqUltrascale%2BMPSoCSecurityFeatures-GeneratingRSAKeypair
