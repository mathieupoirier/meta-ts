# Xilinx kv260 and SOMs

COMPATIBLE_MACHINE:zynqmp-kria-starter = "zynqmp-kria-starter"
COMPATIBLE_MACHINE:zynqmp-zcu102 = "zynqmp-zcu102"
COMPATIBLE_MACHINE:zynqmp-kria-starter-psa = "zynqmp-kria-starter-psa"

# reverted due to kv260 panic
SRC_URI:append:zynqmp-kria-starter = "\
    file://0001-Revert-refactor-el3-runtime-plat_ic_has_interrupt_ty.patch \
    file://0002-Revert-fix-el3-runtime-leverage-generic-interrupt-co.patch \
"

PV:append:zynqmp-kria-starter = "+reverts"

FILESEXTRAPATHS:prepend:zynqmp-kria-starter := "${THISDIR}/files/zynqmp-kria-starter:"
FILESEXTRAPATHS:prepend:zynqmp-zcu102 := "${THISDIR}/files/zynqmp-zcu102:"
FILESEXTRAPATHS:prepend:zynqmp-kria-starter-psa := "${THISDIR}/files/zynqmp-kria-starter:${THISDIR}/files/zynqmp-kria-starter-psa:"

# Get TF-A version
TFA_USED_VERSION = "${@bb.parse.vars_from_file(d.getVar('FILE', False),d)[1] or '1.0'}"

# For meta-arm nanbield (TF-A 2.9), include the patch
# For meta-arm master (TF-A 2.10), the patch is already upstreamed
SRC_URI:append:zynqmp-kria-starter = "${@bb.utils.contains("TFA_USED_VERSION", "2.9.0", \
                                      " file://0001-zynqmp-fix-limit-BL31_LIMIT.patch", \
                                      "", \
                                      d)}"

SRC_URI:append:zynqmp-kria-starter-psa = "${@bb.utils.contains("TFA_USED_VERSION", "2.9.0", \
                                      " file://0001-zynqmp-fix-limit-BL31_LIMIT.patch \
                                        file://0002-feat-zynqmp-switch-to-xlat_v2.patch \
                                        file://0003-trusted-firmware-a-zynqmp-Add-SPMC-manifest.patch \
                                        file://0004-psci-SMCCC_ARCH_FEATURES-discovery-through-PSCI_FEATURES.patch", \
                                      "", \
                                      d)}"

TFA_DEBUG = "0"
TFA_UBOOT = "0"
#TFA_MBEDTLS = "1"
TFA_BUILD_TARGET = "bl31"
TFA_INSTALL_TARGET = "bl31"
# Enabling Secure-EL1 Payload Dispatcher (SPD)
TFA_SPD:zynqmp-zcu102 = ""
TFA_SPD:zynqmp-kria-starter = "opteed"
TFA_SPD:zynqmp-kria-starter-psa = "spmd"
TFA_SPMD_SPM_AT_SEL2:zynqmp-kria-starter-psa = "0"
TFA_TARGET_PLATFORM = "zynqmp"

EXTRA_OEMAKE:append:zynqmp-kria-starter = " ZYNQMP_CONSOLE=cadence1 \
					    ZYNQMP_ATF_MEM_BASE=0xfffea000 \
					    ZYNQMP_ATF_MEM_SIZE=0x16000 \
					    RESET_TO_BL31=1 LOG_LEVEL=0"

EXTRA_OEMAKE:append:zynqmp-kria-starter-psa = " ZYNQMP_CONSOLE=cadence1 \
                                                ZYNQMP_ATF_MEM_BASE=0x00050000 \
                                                ZYNQMP_ATF_MEM_SIZE=0x30000 \
                                                RESET_TO_BL31=1 LOG_LEVEL=0"

EXTRA_OEMAKE:append:zynqmp-zcu102 = " RESET_TO_BL31=1 "

do_deploy:append() {
    cp ${D}/firmware/bl31.bin ${DEPLOYDIR}
}

do_deploy:append:zynqmp-kria-starter-psa() {
    cp ${D}/firmware/bl31.bin ${DEPLOYDIR}
    cp ${D}/optee_ffa_spmc_manifest.dtb ${DEPLOYDIR}/
}

FILES:${PN} += "/optee_ffa_spmc*"
do_install:append:zynqmp-kria-starter-psa() {
    dtc -I dts -O dtb ${S}/plat/xilinx/zynqmp/fdts/optee_ffa_spmc_manifest.dts >  ${B}/optee_ffa_spmc_manifest.dtb
    install -D -p -m 0644 ${B}/optee_ffa_spmc_manifest.dtb ${D}/optee_ffa_spmc_manifest.dtb
}


addtask deploy before do_build after do_install
