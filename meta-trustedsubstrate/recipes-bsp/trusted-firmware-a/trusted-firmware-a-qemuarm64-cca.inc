# qemuarm64-cca  machines specific TFA support

COMPATIBLE_MACHINE = "qemuarm64-cca"

SRC_URI_TRUSTED_FIRMWARE_A = "git://git.codelinaro.org/linaro/dcap/tf-a/trusted-firmware-a.git;protocol=https"
SRCREV_tfa = "ca674e763bb297b8fb95c90cdb936af0d0fcbdb7"
SRCBRANCH = "v1.0-eac5"

TFA_PLATFORM = "qemu"
TFA_UBOOT = "0"
TFA_BUILD_TARGET:aarch64:qemuall = "clean all fip"
TFA_INSTALL_TARGET = "bl1.bin fip.bin flash.bin"
TFA_MBEDTLS = "0"

DEPENDS:append:aarch64:qemuall = " trusted-firmware-rmm edk2-firmware"

EXTRA_OEMAKE:remove = "BL32=${STAGING_DIR_TARGET}${nonarch_base_libdir}/firmware/tee-header_v2.bin"
EXTRA_OEMAKE:remove = "BL32_EXTRA1=${STAGING_DIR_TARGET}${nonarch_base_libdir}/firmware/tee-pager_v2.bin"
EXTRA_OEMAKE:remove = "BL32_EXTRA2=${STAGING_DIR_TARGET}${nonarch_base_libdir}/firmware/tee-pageable_v2.bin"
EXTRA_OEMAKE:remove = "BL32_RAM_LOCATION=tdram"

EXTRA_OEMAKE:append:aarch64:qemuall = " \
    ENABLE_RME=1 \
    QEMU_USE_GIC_DRIVER=QEMU_GICV3 \
    RMM=${DEPLOY_DIR}/images/${MACHINE}/rmm.img \
    BL33=${DEPLOY_DIR}/images/${MACHINE}/QEMU_EFI.fd \
    LOG_LEVEL=40 \
    "

do_install:prepend() {
    dd if=${BUILD_DIR}/bl1.bin of=${BUILD_DIR}/flash.bin
    dd if=${BUILD_DIR}/fip.bin of=${BUILD_DIR}/flash.bin seek=64 bs=4096
}
