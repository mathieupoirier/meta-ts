# Machine specific TFAs

FILESEXTRAPATHS:prepend := "${THISDIR}/files/:"

SRC_URI += "\
    file://0001-trusted-firmware-a-mbedtls_common.mk-remove-hash_inf.patch \
"

# Enable mbed TLS support
TFA_MBEDTLS = "1"

# try to find correct libcrypto.so.3
do_compile:prepend() {
    export LD_LIBRARY_PATH="${STAGING_LIBDIR_NATIVE}"
}

EXTRA_OEMAKE += " LOG_LEVEL=30"

MACHINE_TFA_REQUIRE ?= ""

MACHINE_TFA_REQUIRE:synquacer = "trusted-firmware-a-synquacer.inc"
MACHINE_TFA_REQUIRE:rockpi4b = "trusted-firmware-a-rockpi4b.inc"
MACHINE_TFA_REQUIRE:zynqmp-kria-starter = "trusted-firmware-a-zynqmp.inc"
MACHINE_TFA_REQUIRE:zynqmp-zcu102 = "trusted-firmware-a-zynqmp.inc"
MACHINE_TFA_REQUIRE:tsqemuarm64-secureboot = "trusted-firmware-a-tsqemuarm64-secureboot.inc"
MACHINE_TFA_REQUIRE:tsqemuarm-secureboot = "trusted-firmware-a-tsqemuarm-secureboot.inc"
MACHINE_TFA_REQUIRE:imx8mp-verdin = "trusted-firmware-a-imx8mp-verdin.inc"
MACHINE_TFA_REQUIRE:zynqmp-kria-starter-psa = "trusted-firmware-a-zynqmp.inc"
MACHINE_TFA_REQUIRE:qemuarm64-cca = "trusted-firmware-a-qemuarm64-cca.inc"

require ${MACHINE_TFA_REQUIRE}
