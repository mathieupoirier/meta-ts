From 626c31d03c65efa4283c2a6c2883d099a0739f87 Mon Sep 17 00:00:00 2001
From: Mikko Rapeli <mikko.rapeli@linaro.org>
Date: Thu, 21 Mar 2024 17:58:21 +0000
Subject: [PATCH 1/2] Revert "refactor(el3-runtime): plat_ic_has_interrupt_type
 returns bool"

This reverts commit 1f6bb41dd951714b47bf07bb9a332346ca261033.

Causes panic on kv260

Upstream-Status: Pending

Signed-off-by: Mikko Rapeli <mikko.rapeli@linaro.org>
---
 bl31/ehf.c                                    |  4 ++--
 bl31/interrupt_mgmt.c                         |  2 +-
 .../platform-interrupt-controller-API.rst     | 20 +++++++++----------
 include/plat/common/platform.h                |  2 +-
 plat/common/plat_gicv2.c                      |  6 +++---
 plat/common/plat_gicv3.c                      |  6 +++---
 6 files changed, 20 insertions(+), 20 deletions(-)

diff --git a/bl31/ehf.c b/bl31/ehf.c
index 6f3d9412eb..b328380d1c 100644
--- a/bl31/ehf.c
+++ b/bl31/ehf.c
@@ -1,5 +1,5 @@
 /*
- * Copyright (c) 2017-2023, Arm Limited and Contributors. All rights reserved.
+ * Copyright (c) 2017-2022, ARM Limited and Contributors. All rights reserved.
  *
  * SPDX-License-Identifier: BSD-3-Clause
  */
@@ -458,7 +458,7 @@ void __init ehf_init(void)
 	int ret __unused;
 
 	/* Ensure EL3 interrupts are supported */
-	assert(plat_ic_has_interrupt_type(INTR_TYPE_EL3));
+	assert(plat_ic_has_interrupt_type(INTR_TYPE_EL3) != 0);
 
 	/*
 	 * Make sure that priority water mark has enough bits to represent the
diff --git a/bl31/interrupt_mgmt.c b/bl31/interrupt_mgmt.c
index 68c7f10add..1c26834ff8 100644
--- a/bl31/interrupt_mgmt.c
+++ b/bl31/interrupt_mgmt.c
@@ -47,7 +47,7 @@ static intr_type_desc_t intr_type_descs[MAX_INTR_TYPES];
  ******************************************************************************/
 static int32_t validate_interrupt_type(uint32_t type)
 {
-	if (plat_ic_has_interrupt_type(type)) {
+	if (plat_ic_has_interrupt_type(type) != 0) {
 		return 0;
 	}
 
diff --git a/docs/components/platform-interrupt-controller-API.rst b/docs/components/platform-interrupt-controller-API.rst
index 4de39d1e5a..069c87b84c 100644
--- a/docs/components/platform-interrupt-controller-API.rst
+++ b/docs/components/platform-interrupt-controller-API.rst
@@ -120,39 +120,39 @@ This API should set the priority of the interrupt specified by first parameter
 In case of Arm standard platforms using GIC, the implementation of the API
 writes to GIC *Priority Register* set interrupt priority.
 
-Function: bool plat_ic_has_interrupt_type(unsigned int type); [optional]
-~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
+Function: int plat_ic_has_interrupt_type(unsigned int type); [optional]
+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
 ::
 
     Argument : unsigned int
-    Return   : bool
+    Return   : int
 
 This API should return whether the platform supports a given interrupt type. The
 parameter ``type`` shall be one of ``INTR_TYPE_EL3``, ``INTR_TYPE_S_EL1``, or
 ``INTR_TYPE_NS``.
 
 In case of Arm standard platforms using GICv3, the implementation of the API
-returns *true* for all interrupt types.
+returns ``1`` for all interrupt types.
 
-In case of Arm standard platforms using GICv2, the API always return *true* for
+In case of Arm standard platforms using GICv2, the API always return ``1`` for
 ``INTR_TYPE_NS``. Return value for other types depends on the value of build
 option ``GICV2_G0_FOR_EL3``:
 
 - For interrupt type ``INTR_TYPE_EL3``:
 
-  - When ``GICV2_G0_FOR_EL3`` is ``0``, it returns *false*, indicating no support
+  - When ``GICV2_G0_FOR_EL3`` is ``0``, it returns ``0``, indicating no support
     for EL3 interrupts.
 
-  - When ``GICV2_G0_FOR_EL3`` is ``1``, it returns *true*, indicating support for
+  - When ``GICV2_G0_FOR_EL3`` is ``1``, it returns ``1``, indicating support for
     EL3 interrupts.
 
 - For interrupt type ``INTR_TYPE_S_EL1``:
 
-  - When ``GICV2_G0_FOR_EL3`` is ``0``, it returns *true*, indicating support for
+  - When ``GICV2_G0_FOR_EL3`` is ``0``, it returns ``1``, indicating support for
     Secure EL1 interrupts.
 
-  - When ``GICV2_G0_FOR_EL3`` is ``1``, it returns *false*, indicating no support
+  - When ``GICV2_G0_FOR_EL3`` is ``1``, it returns ``0``, indicating no support
     for Secure EL1 interrupts.
 
 Function: void plat_ic_set_interrupt_type(unsigned int id, unsigned int type); [optional]
@@ -306,4 +306,4 @@ masks out the interrupt ID field from the acknowledged value from GIC.
 
 --------------
 
-*Copyright (c) 2017-2023, Arm Limited and Contributors. All rights reserved.*
+*Copyright (c) 2017-2019, Arm Limited and Contributors. All rights reserved.*
diff --git a/include/plat/common/platform.h b/include/plat/common/platform.h
index 4d1b1c17cf..8cf4ec2035 100644
--- a/include/plat/common/platform.h
+++ b/include/plat/common/platform.h
@@ -125,7 +125,7 @@ int plat_ic_is_sgi(unsigned int id);
 unsigned int plat_ic_get_interrupt_active(unsigned int id);
 void plat_ic_disable_interrupt(unsigned int id);
 void plat_ic_enable_interrupt(unsigned int id);
-bool plat_ic_has_interrupt_type(unsigned int type);
+int plat_ic_has_interrupt_type(unsigned int type);
 void plat_ic_set_interrupt_type(unsigned int id, unsigned int type);
 void plat_ic_set_interrupt_priority(unsigned int id, unsigned int priority);
 void plat_ic_raise_el3_sgi(int sgi_num, u_register_t target);
diff --git a/plat/common/plat_gicv2.c b/plat/common/plat_gicv2.c
index f78d2dfc6b..817f43a3be 100644
--- a/plat/common/plat_gicv2.c
+++ b/plat/common/plat_gicv2.c
@@ -193,9 +193,9 @@ void plat_ic_set_interrupt_priority(unsigned int id, unsigned int priority)
 	gicv2_set_interrupt_priority(id, priority);
 }
 
-bool plat_ic_has_interrupt_type(unsigned int type)
+int plat_ic_has_interrupt_type(unsigned int type)
 {
-	bool has_interrupt_type = false;
+	int has_interrupt_type = 0;
 
 	switch (type) {
 #if GICV2_G0_FOR_EL3
@@ -204,7 +204,7 @@ bool plat_ic_has_interrupt_type(unsigned int type)
 	case INTR_TYPE_S_EL1:
 #endif
 	case INTR_TYPE_NS:
-		has_interrupt_type = true;
+		has_interrupt_type = 1;
 		break;
 	default:
 		/* Do nothing in default case */
diff --git a/plat/common/plat_gicv3.c b/plat/common/plat_gicv3.c
index baa70e0ba1..29c85f49ec 100644
--- a/plat/common/plat_gicv3.c
+++ b/plat/common/plat_gicv3.c
@@ -235,14 +235,14 @@ void plat_ic_set_interrupt_priority(unsigned int id, unsigned int priority)
 	gicv3_set_interrupt_priority(id, plat_my_core_pos(), priority);
 }
 
-bool plat_ic_has_interrupt_type(unsigned int type)
+int plat_ic_has_interrupt_type(unsigned int type)
 {
 	if ((type == INTR_TYPE_EL3) || (type == INTR_TYPE_S_EL1) ||
 			(type == INTR_TYPE_NS)) {
-		return true;
+		return 1;
 	}
 
-	return false;
+	return 0;
 }
 
 void plat_ic_set_interrupt_type(unsigned int id, unsigned int type)
-- 
2.34.1

