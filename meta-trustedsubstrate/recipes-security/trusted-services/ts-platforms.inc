COMPATIBLE_MACHINE:zynqmp-kria-starter-psa = "zynqmp-kria-starter-psa"

FILESEXTRAPATHS:prepend:zynqmp-kria-starter-psa := "${THISDIR}/files/zynqmp-kria-starter-psa:"

SRC_URI:append:zynqmp-kria-starter-psa  = " \
    file://0001-add-boot-order-into-SP-manifest.patch \
    "
