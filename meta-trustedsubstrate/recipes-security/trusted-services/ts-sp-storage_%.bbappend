require ts-platforms.inc

# Override configuration to use block storage service
OECMAKE_SOURCEPATH:zynqmp-kria-starter-psa = "${S}/deployments/protected-storage/config/shared-flash-${TS_ENV}"
