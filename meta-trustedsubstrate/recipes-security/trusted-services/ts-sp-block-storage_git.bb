# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

DESCRIPTION = "Trusted Services block storage service provider"

require recipes-security/trusted-services/ts-sp-common.inc
require ts-platforms.inc

SP_UUID = "63646e80-eb52-462f-ac4f-8cdf3987519c"
TS_SP_BLOCK_STORAGE_CONFIG ?= "default"

OECMAKE_SOURCEPATH="${S}/deployments/block-storage/config/${TS_SP_BLOCK_STORAGE_CONFIG}-${TS_ENV}"
