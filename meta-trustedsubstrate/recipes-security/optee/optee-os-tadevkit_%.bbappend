# Machine specific configurations

MACHINE_OPTEE_OS_REQUIRE ?= ""
MACHINE_OPTEE_OS_REQUIRE:synquacer = "optee-os-tadevkit-synquacer.inc"
MACHINE_OPTEE_OS_REQUIRE:rockpi4b = "optee-os-tadevkit-rockpi4b.inc"

require ${MACHINE_OPTEE_OS_REQUIRE}

# default now in meta-arm, not ok for meta-ts firmware
EXTRA_OEMAKE:remove = "CFG_MAP_EXT_DT_SECURE=y"

EXTRA_OEMAKE += "${@bb.utils.contains('MACHINE_FEATURES', 'optee-stmm', \
    'CFG_STMM_PATH=${DEPLOY_DIR_IMAGE}/uefi.bin', '', d)}"
