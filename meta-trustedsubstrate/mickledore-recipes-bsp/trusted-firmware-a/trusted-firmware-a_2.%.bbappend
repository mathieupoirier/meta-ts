# workaround linker errors:
# bl31.elf has a LOAD segment with RWX permissions
# missing .note.GNU-stack section implies executable stack NOTE: This behaviour is deprecated and will be removed in a future version of the linker
EXTRA_OEMAKE += "LDFLAGS='--no-warn-rwx-segments -z noexecstack'"

